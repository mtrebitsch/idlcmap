def get_map(i,reverse=False):
    from numpy import loadtxt
    from matplotlib.colors import ListedColormap
    from matplotlib.cm import register_cmap
    from os.path import dirname

    rgb,cmap,name = None,None,None

    try:
        rgb = loadtxt("%s/idl%d.txt"%(dirname(__file__),i))
    except:
        print "File idl%d.txt does not exist"%i

    if (rgb != None):
        name = "idl%d"%i
        if reverse:
            rgb = rgb[::-1]
        cmap = ListedColormap(rgb/255., name=name)

        register_cmap(name=name, cmap=cmap)
    return cmap
